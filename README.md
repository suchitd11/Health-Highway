# Health-Highway 


Another frontend of website Health Highway Developed by me!

Health Highway is an IIT-based startup that aims to provide a systematic Yoga experience to Indian customers. We are in the process of developing a web and Mobile app where the customer can book a yoga session at any time according to their convenience. All yoga routines will be designed uniquely for each customer to suit their individual needs and lifestyle. 

Made by using HTML, CSS, Bootstrap.

![image](https://user-images.githubusercontent.com/56268987/125208467-379ba200-e2b0-11eb-9ffc-43ab3869d2cb.png)
